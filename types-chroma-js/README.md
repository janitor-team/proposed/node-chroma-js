# Installation
> `npm install --save @types/chroma-js`

# Summary
This package contains type definitions for Chroma.js (https://github.com/gka/chroma.js).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/chroma-js.

### Additional Details
 * Last updated: Wed, 23 Dec 2020 14:40:35 GMT
 * Dependencies: none
 * Global values: `chroma`

# Credits
These definitions were written by [Sebastian Brückner](https://github.com/invliD), and [Marcin Pacholec](https://github.com/mpacholec).
